import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor(private httpService: HttpClient) { }
  arrTable: string [];

  ngOnInit() {
    this.httpService.get('./assets/sampledata.json').subscribe(
      data => {
        this.arrTable = data as string [];
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
    }

}
